import React, { Component } from "react";
import { Text, View, SafeAreaView, ImageBackground } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import InputText from "../SmartComponent/InputText";
import ButtonView from "../SmartComponent/ButtonView";
import { forgotpassword } from "../Utils/apiconfig";

export default class ForgotPassword extends Component {
  constructor() {
    super();
    this.state = {
      ErrorEmail: null,
      ErrorUserEmail: null,
      email: null,
    };
  }
  onHandleForgotPassword = async () => {
    // this.props.navigation.navigate('SuccessPage', {register: false});

    const error = this.Validation();
    if (!error) {
      const { email } = this.state;
      this.setState({ isLoading: true });
      let data = {
        EmailID: email,
        Type: 1,
      };
      console.log(data);
      await forgotpassword(data)
        .then((res) => {
          console.log("res: ", JSON.stringify(res));
          console.log("res:123", res[0].UserCode);
          this.setState({ isLoading: false });
          if (res[0].UserCode === "Sucesss") {
            alert(
              "Link sent successfully. Please check your registered email."
            );
          }
          if (res[0].UserCode === "Error") {
            alert("Please check your email.");
          }
          this.props.navigation.navigate("SuccessPage", { register: false });
        })
        .catch((error) => {
          if (error.response) {
            console.log("responce_error", error.response);
            this.setState({ isLoading: false });
          } else if (error.request) {
            this.setState({ isLoading: false });
            console.log("request error", error.request);
          }
        });
    }
  };
  Validation = () => {
    const { email } = this.state;
    this.setState({ isLoading: false });
    // debugger;
    const invalidFields = [];

    if (!email) {
      invalidFields.push("email");
      this.setState({ ErrorEmail: "Email address is required" });
    } else {
      this.setState({ ErrorEmail: null });
    }

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(email) === false && email) {
      invalidFields.push("ErrorUserEmail");
      this.setState({ ErrorUserEmail: "Please enter valid email" });
    } else {
      this.setState({ ErrorUserEmail: null });
    }
    return invalidFields.length > 0;
  };
  render() {
    const { ErrorEmail, ErrorUserEmail, email } = this.state;
    return (
      <View>
        <ImageBackground
          source={require("../../assets/plan_app_images/bg/forgot-password.jpg")}
          resizeMode="cover"
          style={{ height: "100%" }}
        >
          <SafeAreaView style={{ height: "100%" }}>
            <ScrollView keyboardShouldPersistTaps="always">
              <View style={{ paddingHorizontal: 34, marginTop: 150 }}>
                <Text
                  style={{
                    color: "#53a20a",
                    fontSize: 35,
                    fontFamily: "Roboto-Medium",
                    lineHeight: 50,
                  }}
                >
                  Lost Your Password!
                </Text>
                <Text style={{ fontSize: 15, color: "gray" }}>
                  Please enter your username or email address. You will receive
                  a link to create a new password via email.
                </Text>
              </View>
              <InputText
                title={" Email Address"}
                onChangeText={(email) => this.setState({ email })}
                error={ErrorEmail || ErrorUserEmail}
                value={email}
              />
              <View style={{ marginTop: 20, marginLeft: 20 }}>
                <ButtonView
                  onPress={() => this.onHandleForgotPassword()}
                  title="Reset Password"
                />
              </View>
            </ScrollView>
          </SafeAreaView>
        </ImageBackground>
      </View>
    );
  }
}
